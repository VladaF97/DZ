package TestPackage2;


import java.util.Scanner;

public class MyFirstClass {
    public static void main(String []args) {
        //комментарий для примера
        Scanner sc = new Scanner(System.in); //чтобы знать где оставлять комментарии
        /*
        Тестирую блочный комментарий
         */
        int a = sc.nextInt();
        System.out.println("Я ввел число: " + a );

        System.out.println("Привет мир!");
        System.out.println("Учу Java и мне очень нравится!");
    }
}
