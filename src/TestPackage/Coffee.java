package TestPackage;

public class Coffee {
    public static void main (String [] args) throws java.lang.Exception
    {
        System.out.println("Кофе-машина");
        int moneyAmount = 120;

        int espressoPrice = 80;
        int cappucinoPrice = 150;
        int waterPrice = 20;

        if (moneyAmount >= espressoPrice) {
            System.out.println("Вы можете купить эспрессо");
        }

        if (moneyAmount >= cappucinoPrice) {
            System.out.println("Вы можете купить капучино");
        }
        if (moneyAmount >= waterPrice) {
            System.out.println("Вы можете купить воду");
        }
    }
}
